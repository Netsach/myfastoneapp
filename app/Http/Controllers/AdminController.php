<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserLoding;
use App\Models\Location;

class AdminController extends Controller
{
        public function index() {
            $users = User::orderBy('id', 'desc')->get();
            return view('dashboards.admins.index', [
                'users' => $users
            ]);
        }
        public function create(){
            return view('dashboards.admins.create');
        }

        public function store(Request $request){
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'role'=>2,
                'track_no'=>Str::random(9),
                'password' => Hash::make($request['password']),
            ]);
            return redirect()->route('admin.create')->with('successMessage', 'The New Client was successfully registered...');
        }

        public function edit ($id) {
           $user = User::where('id', '=', $id)->get();
           return view('dashboards.admins.edit')->with('user', $user);
        }
        
        public function checkDetails ($id) {
            $user = UserLoding::where('user_id', $id)->get();
            return view('dashboards.admins.allSingleLoadings')->with('user', $user);
         }

        public function editForUpdate ($id) {
            $user = UserLoding::where('user_id', '=', $id)->get();
            return view('dashboards.admins.editForUpdate')->with('user', $user);
         }

        public function loadData (Request $request) {
            $user = UserLoding::where('user_id', '=', $request->id)->first();
                if ($user === null) {
                    $user = new UserLoding;
                    $user->user_id = $request->id;
                    $user->name1= $request->name1;
                    $user->delivery_schedule=  $request->delivery_schedule;
                    $user->last_location=  $request->last_location;
                    $user->origin= $request->origin;
                    $user->destination=  $request->destination;
                    $user->service_mode=  $request->service_mode;
                    $user->type_service=  $request->type_service;
                    $user->weight=  $request->weight;
                    $user->collection_date_time=  $request->collection_date_time;
                    $user->shipping_description=  $request->shipping_description;
                    $user->phone1=  $request->phone1;
                    $user->address1=  $request->address1;
                    $user->name2=  $request->name2;
                    $user->phone2=  $request->phone2;
                    $user->address2=  $request->address2;
                    $user->tracking_no=  $request->tracking_no;
                    $user->save();
                    return redirect()->route('admin.dashboard')->with('successMessage', 'The Client Data was loaded successfully...');
                } else {
                    return back()->with('errorMessage', 'This user Exits already...');
                }
       
           
         }
         public function update(Request $request){
            $user = UserLoding::where('id', '=', $request->id)->first();
            $user->name1= $request->name1;
            $user->delivery_schedule=  $request->delivery_schedule;
            $user->last_location=  $request->last_location;
            $user->origin= $request->origin;
            $user->destination=  $request->destination;
            $user->service_mode=  $request->service_mode;
            $user->type_service=  $request->type_service;
            $user->weight=  $request->weight;
            $user->collection_date_time=  $request->collection_date_time;
            $user->shipping_description=  $request->shipping_description;
            $user->phone1=  $request->phone1;
            $user->address1=  $request->address1;
            $user->name2=  $request->name2;
            $user->phone2=  $request->phone2;
            $user->address2=  $request->address2;
            $user->tracking_no=  $request->tracking_no;
            $user->save();
            return redirect()->route('admin.dashboard')->with('successMessage', 'The Client Data was loaded successfully...');
            // return back()->with('successMessage', 'This user Exits already...');
            // return view('dashboards.admins.profile');
        }

        public function profile(){
            return view('dashboards.admins.profile');
        }

        public function newLocation ($id) {
            $user = UserLoding::where('user_id', '=', $id)->get();
            return view('dashboards.admins.newLocation')->with('user', $user);
        }

        public function loadLocation (Request $request) {
                $user = new Location;
                $user->model_id = $request->id;
                $user->track_no = $request->track_no;
                $user->new_location =  $request->new_location;
                $user->state_shipping =  $request->state_shipping;
                $user->create_date= $request->create_date;
                $user->remark=  $request->remark;
                $user->save();
                return redirect()->route('admin.dashboard')->with('successMessage', 'The Client Data was loaded successfully...');
           
        }

        public function viewLocation($id) {
            $users = Location::where('model_id', '=', $id)->get();
            return view('dashboards.admins.locationHistory', [
                'users' => $users
            ]);
        }

        public function destroy ($id) {
            $user = User::find($id);
            $user->delete();
            return redirect()->route('admin.dashboard')->with('successMessage', 'The Client Data was loaded successfully...');
        }
}
