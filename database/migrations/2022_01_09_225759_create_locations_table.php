<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedInteger('model_id');
            $table->string('track_no');
            $table->string('new_location');
            $table->string('state_shipping');
            $table->datetime('create_date'); 
            $table->string('remark'); 
            $table->timestamps();
            $table->foreign('model_id')
            ->references('id')
            ->on('user_loadings')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
