
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>Fastone Express Cargo Delivery service company registered in England and Wales</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Convey a Transportation Category Bootstrap Responsive Web Template  Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
         Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
      <script type="application/x-javascript">
         addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
         function hideURLbar(){ window.scrollTo(0,1); }
      </script>
      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!--stylesheets-->
      <link href="{{ asset('assets/css/style.css') }}" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets--> 
      <!-- font-awesome icons -->
      <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
      <!-- //font-awesome icons -->
      <link rel="stylesheet" href="{{ asset('assets/css/flexslider.css') }}" type="text/css" media="screen" />
      <!-- partner js-->
      <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
      @yield('styles')
   </head>
   <body>
    <div class="header-w3layouts">
        <div class="container">
           <div class="header-bar">
              <nav class="navbar navbar-default">
                 <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <h1><a class="navbar-brand" href="/">FOEC
                       </a>
                    </h1>
                 </div>
                 <!-- Collect the nav links, forms, and other content for toggling -->
                 <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav>
                       <ul class="nav navbar-nav">
                          <li class="active"><a href="/"><span class="fa fa-home banner-nav" aria-hidden="true"></span>Home</a></li>
                          <li><a href="{{ route('about') }}"><span class="fa fa-info-circle banner-nav" aria-hidden="true"></span>About</a></li>
                          <li><a href="{{ route('services') }}"><span class="fa fa-cogs banner-nav" aria-hidden="true"></span>Services</a></li>
                          <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-file banner-nav" aria-hidden="true"></span>Track my parcel<span class="caret"></span>
                             </a>
                             <ul class="dropdown-menu" role="menu">
                                <li>
                                   <a href="{{ route('track') }}">Track now</a>
                                </li>
                               
                             </ul>
                          </li>
                          <li><a href="{{ route('contact') }}"><span class="fa fa-envelope-o banner-nav" aria-hidden="true"></span>Contact Us</a></li>
                          
                          @guest
                          @if (Route::has('login'))
                          <li><a href="{{ route('login') }}"><span class="fa fa-info-circle banner-nav" aria-hidden="true"></span>login</a></li>
                        
                          @endif
                          @if (Route::has('register'))
                          <li><a href="{{ route('register') }}"><span class="fa fa-info-circle banner-nav" aria-hidden="true"></span>Register</a></li>
                          {{-- <li><a href="" id="google_translate_element" style="width: 5px;margin-right:50px"></a></li> --}}
                          {{-- <li id="google_translate_element" style="height: 10px;"></li> --}}
                          @endif
                          @else
                          <li><a href="{{ route('auth.logout') }}"><span class="fa fa-envelope-o banner-nav" aria-hidden="true"></span>Logout</a></li>
                          @endguest
                        </ul>
                    </nav>
                 </div>
              </nav>
           </div>
           <div class="clearfix"> </div>
        </div>
     </div>


     @yield('content')



      <!--footer -->
      <footer>
        <div class="container">
           <div class="col-md-6 col-sm-7 header-side">
              <div class="header-side">
                 <div class="buttom-social-grids">
                    <ul>
                       <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                       <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                       <li><a href="#"><span class="fa fa-rss"></span></a></li>
                       <li><a href="#"><span class="fa fa-vk"></span></a></li>
                    </ul>
                 </div>
              </div>
              <p>©2022 fastoneexpresscargo. All Rights Reserved<a href="#" target="_blank"></a></p>
           </div>
           <div class="col-md-6 col-sm-5 header-right">
              <h2><a href="/">FOEC</a></h2>
           </div>
        </div>
     </footer>
     <!--//footer -->

       <!--js working-->
       <script  src='{{ asset('assets/js/jquery-2.2.3.min.js') }}'></script>
       <!-- //js  working-->
       <!-- banner-->
       <script src="{{ asset('assets/js/responsiveslides.min.js') }}"></script>
       <script>
          // You can also use "$(window).load(function() {"
          $(function () {
            // Slideshow 4
          $("#slider4").responsiveSlides({
              auto: true,
              pager: false,
              nav: true,
              speed: 900,
              namespace: "callbacks",
              before: function () {
                $('.events').append("<li>before event fired.</li>");
              },
              after: function () {
                $('.events').append("<li>after event fired.</li>");
              }
            });
          
          });
       </script>
       <!--// banner-->
       <!--partner-->
       <script src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
       <!--Start-slider-script-->
       <script>
          $(window).load(function(){
              $('.flexslider').flexslider({
              animation: "slide",
              start: function(slider){
                  $('body').removeClass('loading');
              }
              });
          });
       </script>
       <!--End-slider-script partner-->
       <!-- start-smoth-scrolling -->
       <script src="{{ asset('assets/js/move-top.js') }}"></script>
       <script src="{{ asset('assets/js/easing.js') }}"></script>
       <script>
          jQuery(document).ready(function ($) {
              $(".scroll").click(function (event) {
                  event.preventDefault();
                  $('html,body').animate({
                      scrollTop: $(this.hash).offset().top
                  }, 1000);
              });
          });
       </script>
       <!-- start-smoth-scrolling -->
       <!-- for-bottom-to-top smooth scrolling -->
       <script>
          $(document).ready(function () {
              /*
                  var defaults = {
                  containerID: 'toTop', // fading element id
                  containerHoverID: 'toTopHover', // fading element hover id
                  scrollSpeed: 1200,
                  easingType: 'linear' 
                  };
              */
              $().UItoTop({
                  easingType: 'easeOutQuart'
              });
          });
       </script>
       <script type="text/javascript">
         function googleTranslateElementInit() {
           new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
         }
         </script>
         <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
       <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
       <!-- //for-bottom-to-top smooth scrolling -->
       <!-- bootstrap-->
       <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
     @yield('scripts')
   </body>

</html>