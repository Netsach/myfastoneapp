@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')
<div class="contact" id="contact">
    <div class="container">
                <h3 class="title">Register</h3>
                <div class="col-md-12 contact-us">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                    <div class="styled-input">
                        <input id="name" type="text" placeholder="Name"  name="name" value="" required autofocus>
                    </div>
                    <div class="styled-input">
                        <input id="email" type="email" placeholder="Email"  name="email" value="" required autocomplete="email" autofocus>
                    </div>
                    <div class="styled-input">
                            <input id="password" type="text" @error('password') is-invalid @enderror" name="password" placeholder="password" required autocomplete="new-password">
                    </div>
                    <div class="styled-input">
                    <input id="password-confirm" type="text" placeholder="password_confirmation"  name="password_confirmation" required autocomplete="new-password">
                    </div>
                    <br>
                    <div>
                        <div class="click">
                            <input type="submit" name="submit" value="Register">
                        </div>
                    </div>
                </form>
                </div>
    </div>
    @include('includes.whatsapp')
</div>
@endsection
