<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserLoding;
use App\Models\Location;

class UserController extends Controller
{
        public function index(){
            $user = auth()->user()->id;
            $user = UserLoding::where('user_id', $user)->get();
            return view('dashboards.users.index')->with('user', $user);
        }
        public function viewLocationForUser($id) {
            $users = Location::where('model_id', '=', $id)->get();
            return view('dashboards.users.locationHistory', [
                'users' => $users
            ]);
        }
        public function profile(){
            return view('dashboards.users.profile');
        }
}
