@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')
        <div class="contact" id="contact">
            <div class="container">
                <h3 class="title">Contact</h3>
                <div class="col-md-6 contact-us">
                   <form method="POST" action="{{ route('mailContact') }}">
                     @csrf
                     @include('includes.messages')
                      <div class="styled-input">
                         <input type="text" name="name" placeholder="Name" required="">
                      </div>
                      <div class="styled-input">
                         <input type="email" name="email" placeholder="Email" required=""> 
                      </div>
                      <div class="styled-input">
                         <textarea name="body" placeholder="Message" required=""></textarea>
                      </div>
                      <div>
                         <div class="click">
                            <input type="submit" name="submit" value="SEND">
                         </div>
                      </div>
                   </form>
                </div>

                <div class="col-md-6 contactright">
                    <h3>WHERE TO FIND US</h3>
                    <div class="footer_grid_left">
                       <div class="contact_footer_grid_left">
                          <i class="fa fa-map-marker" aria-hidden="true"></i>
                       </div>
                       <p>fastone express cargo,No. 95, HELICONIE BEA, Medellin, Colombie, 057 Antioquia Medellin, C31 </p>
                    </div>
                    <div class="footer_grid_left">
                       <div class="contact_footer_grid_left">
                          <i class="fa fa-phone" aria-hidden="true"></i>
                       </div>
                       <p>Company Number: +1 (530) 414-9660, Registered in England and Wales,
                 VAT Number: +1 (530) 414-9660</p>
                    </div>
                    <div class="footer_grid_left">
                       <div class="contact_footer_grid_left">
                          <i class="fa fa-envelope-o" aria-hidden="true"></i>
                       </div>
                       <p><a href="mailto:worldfastestdeliveryservice@gmail.com">worldfastestdeliveryservice@gmail.com</a> 
                          <span><a href="mailto:worldfastestdeliveryservice@gmail.com">worldfastestdeliveryservice@gmail.com</a></span>
                          <span><a href="#">support@fastoneexpresscargo.com</a></span>
                       </p>
                    </div>
                 </div>
                 @include('includes.whatsapp')
                <div class="clearfix"> </div>
            </div>
        </div>
            
@endsection

