@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')
<div class="contact" id="contact">
    <div class="container">
                <h3 class="title">Login</h3>
                <div class="col-md-6 contact-us">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        @include('includes.messages')
                
                    <div class="styled-input">
                        <input id="email" type="email" placeholder="Email"  name="email" value="" required autocomplete="email" autofocus>
                    </div>
                    <div class="styled-input">
                            <input id="password" type="text" @error('password') is-invalid @enderror" name="password" placeholder="password" required autocomplete="new-password">
                    </div>
                   <br>
                    <div>
                        <div class="click">
                            <input type="submit" name="submit" value="Login">
                        </div>
                    </div>
                </form>
                </div>


                <div class="col-md-6 contactright">
                    <h3>WHERE TO FIND US</h3>
                     <div class="footer_grid_left">
                        <div class="contact_footer_grid_left">
                           <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <p>fastone express cargo,No. 95, HELICONIE BEA, Medellin, Colombie, 057 Antioquia Medellin, C31 </p>
                     </div>
                     <div class="footer_grid_left">
                        <div class="contact_footer_grid_left">
                           <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <p>Company Number: +1 (530) 414-9660, Registered in England and Wales,
                  VAT Number: +1 (530) 414-9660</p>
                     </div>
                     <div class="footer_grid_left">
                        <div class="contact_footer_grid_left">
                           <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        </div>
                        <p><a href="mailto:worldfastestdeliveryservice@gmail.com">worldfastestdeliveryservice@gmail.com</a> 
                           <span><a href="mailto:worldfastestdeliveryservice@gmail.com">worldfastestdeliveryservice@gmail.com</a></span>
                           <span><a href="#">support@fastoneexpresscargo.com</a></span>
                        </p>
                     </div>
                  </div>
                  @include('includes.whatsapp')
                <div class="clearfix"> </div>
    </div>
</div>
@endsection
