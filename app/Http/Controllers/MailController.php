<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\TextMail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function index (Request $request) {

        $details = [ 
            'name' => $request->name,
            'email' => $request->email,
            'body' => $request->body
        ];

        Mail::to("support@fastoneexpresscargo.com")->send(new TextMail($details));
        return back()->with('successMessage', 'You massage have been recieved and we will contact you shortly...');

    }
}
