@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')
<div class="service-inn" id="services-inn">
    <div class="container">
       <h3 class="title">Services</h3>
       <div class="ser-grid-three">
          <div class="col-md-6  col-sm-6 col-xs-6 service-num-info">
             <div class="white-shadow">
                <div class=" col-md-3 col-sm-3 col-xs-3 service-num-left">
                   <h6>1</h6>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 service-info-left">
                   <h4>International Transport Deliver System</h4>
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-6  col-sm-6 col-xs-6 service-num-info">
             <div class="white-shadow">
                <div class=" col-md-3 col-sm-3 col-xs-3 service-num-left">
                   <h6>2</h6>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 service-info-left">
                   <h4>Shipping Services & Logistics Management</h4>
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-6  col-sm-6 col-xs-6  service-num-info">
             <div class="white-shadow">
                <div class=" col-md-3 col-sm-3 col-xs-3 service-num-left">
                   <h6>3</h6>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 service-info-left">
                   <h4>Goods carrying Train</h4>
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-6  col-sm-6 col-xs-6  service-num-info">
             <div class="white-shadow">
                <div class=" col-md-3 col-sm-3 col-xs-3 service-num-left">
                   <h6>4</h6>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 service-info-left">
                   <h4>Truck Logistics Service</h4>
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-6  col-sm-6 col-xs-6 service-num-info">
             <div class="white-shadow">
                <div class=" col-md-3 col-sm-3 col-xs-3 service-num-left">
                   <h6>5</h6>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 service-info-left">
                   <h4>Auto Shipping</h4>
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-6  col-sm-6 col-xs-6 service-num-info">
             <div class="white-shadow">
                <div class=" col-md-3 col-sm-3 col-xs-3 service-num-left">
                   <h6>6</h6>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 service-info-left">
                   <h4>Auto Shipping</h4>
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="clearfix"> </div>
       </div>
    </div>
 </div>
 <div class="ser-img-grids">
    <div class="container">
       <div class="blog-top-grids">
          <div class="col-md-7 service-two-grid left-side-ser">
             <h4>AIRWAYS TRANSPORT
             </h4>
             <p class="groom-right">Fastone express cargo delivery service provide the best and secure air transport
             with over 60+ couriers across the globe. 
             </p>
             <div class="w3layouts_more-buttn">
                <a href="#">Read More</a>
             </div>
          </div>
          <div class="col-md-5 ser-gr">
          </div>
          <div class="clearfix"> </div>
          <div class="image-low-gr">
             <div class="blog-top-grids low-grid">
                <div class="col-md-5 ser-br">
                </div>
                <div class="col-md-7 service-two-grid imgser-right-side">
                   <h4>WATERWAYS TRANSPORT
                   </h4>
                   <p>We provide the best in waterways transport both local and international as we are equiped with 30+ cargoes shippment machines. 
                   </p>
                   <div class="w3layouts_more-buttn">
                      <a href="#">Read More</a>
                   </div>
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 <div class="service-inn" >
    <div class="container">
       <div class="service-bottom-girds ">
          <div class="col-md-4 col-sm-6 col-xs-6 its-banner-ser">
             <div class="services-clr">
                <div class=" service-icon-grid">
                   <span class="fa fa-thumbs-o-up  banner-icon" aria-hidden="true"></span>
                   <h4>Best Service</h4>
                  
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-6 its-banner-ser">
             <div class="services-clr">
                <div class=" service-icon-grid">
                   <span class="fa fa-users banner-icon" aria-hidden="true"></span>
                   <h4>Support Team</h4>
                  
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-6 its-banner-ser">
             <div class="services-clr">
                <div class=" service-icon-grid">
                   <span class="fa fa-line-chart banner-icon" aria-hidden="true"></span>
                   <h4>Fast &amp; Efficient</h4>
                   
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-6 its-banner-ser top-ser">
             <div class="services-clr">
                <div class=" service-icon-grid">
                   <span class="fa fa-plane banner-icon" aria-hidden="true"></span>
                   <h4>Airway Service</h4>
                  
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-6 its-banner-ser top-ser">
             <div class="services-clr">
                <div class=" service-icon-grid">
                   <span class="fa fa-road banner-icon" aria-hidden="true"></span>
                   <h4>Roadway Service</h4>
                   
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-6 its-banner-ser top-ser">
             <div class="services-clr">
                <div class=" service-icon-grid">
                   <span class="fa fa-ship banner-icon" aria-hidden="true"></span>
                   <h4>Shipping Service</h4>
                   
                </div>
                <div class="clearfix"> </div>
             </div>
          </div>
          @include('includes.whatsapp')
          <div class="clearfix"> </div>
       </div>
    </div>
 </div>
 <!--testimonials-->
@endsection