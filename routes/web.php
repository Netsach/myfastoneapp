<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
});

Route::middleware(['middleware' => 'PreventBackHistory'])->group(function () {
    Auth::routes();
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/contact', [App\Http\Controllers\ContactController::class, 'index'])->name('contact');
Route::get('/track', [App\Http\Controllers\TrackController::class, 'index'])->name('track');
Route::post('/trackData', [App\Http\Controllers\TrackController::class, 'trackData'])->name('trackData');
Route::get('/services', [App\Http\Controllers\ServicesController::class, 'index'])->name('services');
Route::get('/about', [App\Http\Controllers\AboutController::class, 'index'])->name('about');
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])
->name('auth.logout');

Route::post('mailContact', [App\Http\Controllers\MailController::class, 'index'])
->name('mailContact');


Route::get('trackLocation/{id}', [App\Http\Controllers\TrackController::class, 'trackLocation'])->name('trackLocation');


Route::group(['prefix'=>'admin', 'middleware' =>['isAdmin', 'auth', 'PreventBackHistory']], function () {
    Route::get('dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('create', [AdminController::class, 'create'])->name('admin.create');
    Route::get('profile', [AdminController::class, 'profile'])->name('admin.profile');
     Route::post('create', [AdminController::class, 'store'])->name('admin.create_client');
     Route::get('edit/{id}', [AdminController::class, 'edit'])->name('admin.edit');
     Route::post('loadData', [AdminController::class, 'loadData'])->name('admin.loadData');
    Route::get('editForUpdate/{id}', [AdminController::class, 'editForUpdate'])->name('admin.editForUpdate');
    Route::get('check/{id}', [AdminController::class, 'checkDetails'])->name('admin.check');
    Route::post('update', [AdminController::class, 'update'])->name('admin.update');
    Route::get('newLocation/{id}', [AdminController::class, 'newLocation'])->name('admin.newLocation');
    Route::post('loadLocation', [AdminController::class, 'loadLocation'])->name('admin.loadLocation');
    Route::get('viewLocation/{id}', [AdminController::class, 'viewLocation'])->name('admin.viewLocation');
    Route::delete('delete/{id}', [AdminController::class, 'destroy'])->name('admin.destroy');
});


Route::group(['prefix'=>'user', 'middleware' =>['isUser', 'auth','PreventBackHistory']], function () {
    Route::get('dashboard', [UserController::class, 'index'])->name('user.dashboard');
    Route::get('profile', [UserController::class, 'profile'])->name('user.profile');
    Route::get('userLocation/{id}', [UserController::class, 'viewLocationForUser'])->name('user.viewLocation');
});