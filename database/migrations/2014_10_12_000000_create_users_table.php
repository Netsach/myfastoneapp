<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('track_no')->unique();
            $table->boolean('role')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_loadings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('delivery_schedule');
            $table->string('last_location');
            $table->string('origin');
            $table->string('destination');
            $table->string('service_mode');
            $table->string('type_service');
            $table->string('weight');
            $table->datetime('collection_date_time');
            $table->string('shipping_description');
            $table->string('name1');
            $table->string('phone1');
            $table->string('address1');
            $table->string('name2');
            $table->string('phone2');
            $table->string('address2');
            $table->string('tracking_no');
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
