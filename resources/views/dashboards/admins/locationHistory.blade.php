@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')

<section id="tables">
    <div class="page-header">
            <div class="bs-docs-example" style="margin: 20px">
                <table class="table">
                    <thead>
                       <tr>
                          <th>Tracking No</th>
                          <th>New Location</th>
                          <th>State Shipping</th>
                          <th>Date and time</th>
                          <th>Remark</th>
                       </tr>
                    </thead>

                    <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td><a href="#">{{ $user->track_no }}</a></td>
                                    <td><a href="#">{{ $user->new_location }}</a></td>
                                    <td><a href="#">{{ $user->state_shipping  }}</a></td>
                                    <td><a href="">{{ $user->create_date }}</a></td>
                                    <td><a href="">{{ $user->remark  }}</a></td>
                                </tr>
                                @endforeach
                      
                        </tbody>
                </table>
            </div>
            <!--- /here --->
            @include('includes.whatsapp')
    </div>
</section>

@endsection

@section('scripts')
<script src='{{ asset('assets/js/jquery-2.2.3.min.js') }}'></script>
<!-- //js  working-->
<!-- start-smoth-scrolling -->
<script src="{{ asset('assets/js/move-top.js') }}"></script>
<script src="{{ asset('assets/js/easing.js') }}"></script>
<script>
   jQuery(document).ready(function ($) {
       $(".scroll").click(function (event) {
           event.preventDefault();
           $('html,body').animate({
               scrollTop: $(this.hash).offset().top
           }, 1000);
       });
   });
</script>
<!-- start-smoth-scrolling -->
<!-- for-bottom-to-top smooth scrolling -->
<script>
   $(document).ready(function () {
       /*
           var defaults = {
           containerID: 'toTop', // fading element id
           containerHoverID: 'toTopHover', // fading element hover id
           scrollSpeed: 1200,
           easingType: 'linear' 
           };
       */
       $().UItoTop({
           easingType: 'easeOutQuart'
       });
   });
</script>
@endsection