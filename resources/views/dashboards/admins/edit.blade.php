@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')
    <div class="contact" id="contact">
        <div class="container">
            <div class="container">
                <h3 class="title">PARCEL TRACKING</h3>
                @foreach ($user as $user)   
                <div class="col-md-12 contact-us">
                  <a href="{{ route('admin.dashboard') }}" class="btn btn-primary" style="color: #fff; height:30%">Back</a>
                   <form method="POST" action="{{ route('admin.loadData') }}">
                     @include('includes.messages')
                    @csrf
                      <div class="styled-input">
                         <input type="hidden" name="id" value="{{ $user->id }}"  required="">
                      </div>
                      <h2>Delivery schedule:</h2>
                       <div class="styled-input">
                         <input type="date" name="delivery_schedule" value="" required="" style="width: 100%; height: 50px;"> 
                      </div>
                       <div class="styled-input">
                         <input type="text" name="last_location" placeholder="last location" required=""> 
                      </div>
                      <br>
                      <h2>ADDITIONAL INFORMATION</h2>
                       <div class="styled-input">
                         <input type="text" name="origin" placeholder="originating company" required> 
                      </div>
                       <div class="styled-input">
                         <input type="text" name="destination" placeholder="destination" required> 
                      </div>
                       <div class="styled-input">
                         <input type="text" name="service_mode" placeholder="Service mode" required> 
                      </div>
                       <div class="styled-input">
                         <input type="text" name="type_service" placeholder="Type service" required> 
                      </div>
                      <div class="styled-input">
                         <input type="text" name="weight" placeholder="Weight" required>
                      </div>
                      <br />
                      <h2>Collection date and time:</h2>
                      <div class="styled-input">
                         <input type="date" name="collection_date_time" placeholder="collection date and time" style="width: 100%; height: 50px">
                      </div>
                      <div class="styled-input">
                         <input type="text" name="shipping_description" placeholder="shipping description" required>
                      </div>
                      <br>
                      <h2>Details of the sender</h2>
                      <div class="styled-input">
                         <input type="text" name="name1" value="{{ $user->name }}" placeholder="name" readonly>
                      </div>
                      <div class="styled-input">
                         <input type="text" name="phone1" placeholder="phone" required>
                      </div>
                      <div class="styled-input">
                         <input type="text" name="address1" placeholder="address" required>
                      </div>
                      <br>
                      <h2>Information from the recipient</h2>
                       <div class="styled-input">
                         <input type="text" name="name2" value="" placeholder="name"  required>
                      </div>
                      <div class="styled-input">
                        <input type="text" name="phone2" placeholder="phone" required>
                      </div>
                       <div class="styled-input">
                       <input type="text" name="address2" placeholder="address" required>
                      </div>
                       <div class="styled-input">
                         <input type="text" name="tracking_no" value="{{ $user->track_no }}" readonly>
                      </div>
                      <br>
                      <div>
                         <div class="click">
                            <input type="submit" name="submit" value="Submit">
                         </div>
                      </div>
                   </form>
                </div>

                @endforeach
            </div>
           <!--- /here --->
           @include('includes.whatsapp')
            <div class="clearfix"> </div>
         </div>
    </div>
@endsection