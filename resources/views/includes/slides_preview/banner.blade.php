 <!-- Slideshow 4 -->
 <div class="slider">
    <div class="callbacks_container">
       <ul class="rslides" id="slider4">
          <li>
             <div class="slider-img w3-oneimg">
                <div class="container">
                   <div class="slider-info">
                      <h6>Worldwide</h6>
                      <h4>Biggest<br>Network</h4>
                      <p>We offer a diverse range of transportation services from project cargo to international transportation and domestic retail distribution and delivery.</p>
                   </div>
                </div>
             </div>
          </li>
          <li>
             <div class="slider-img w3-twoimg">
                <div class="container">
                   <div class="slider-info">
                      <h6>The</h6>
                      <h4>International<br>Airway </h4>
                     <p>We offer a diverse range of transportation services from project cargo to international transportation and domestic retail distribution and delivery.</p>
                   </div>
                </div>
             </div>
          </li>
          <li>
             <div class="slider-img w3-threeimg">
                <div class="container">
                   <div class="slider-info">
                      <h6>Supplying</h6>
                      <h4>Secured<br> Transport</h4>
                      <p>We offer a diverse range of transportation services from project cargo to international transportation and domestic retail distribution and delivery.</p>
                   </div>
                </div>
             </div>
          </li>
       </ul>
    </div>
    <div class="clearfix"> </div>
    <!-- This is here just to demonstrate the callbacks -->
    <!-- <ul class="events">
       <li>Example 4 callback events</li>
       </ul>-->
 </div>
 <!--//banner-->