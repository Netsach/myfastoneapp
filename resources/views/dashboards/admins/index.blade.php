@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')

<section id="tables">
    <div class="page-header">
                <a href="{{ route('admin.create') }}" class="btn btn-primary" style="color: #fff; height:30%">Add New Client</a>
            
            <div class="bs-docs-example" style="margin: 20px">
                <table class="table">
                    <thead>
                       <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Tracking no</th>
                          <th>Profile</th>
                          <th>New Location</th>
                          <th>Updates</th>
                          <th>Delete</th>
                       </tr>
                    </thead>

                    <tbody>

                       
                                @foreach ($users as $user)
                                <tr>
                                    <td><a href="{{ route('admin.edit', $user->id) }}" class="btn btn-info" style="color: #fff">{{ $user->name }}</a></td>
                                    <td><a href="">{{ $user->email }}</a></td>
                                    <td><a href="">{{ $user->track_no }}</a></td>
                                    <td><a href="{{ route('admin.check', $user->id) }}" class="btn btn-primary" style="color: #fff; height:40%">Check Profile</a></td>
                                    <td><a href="{{ route('admin.newLocation', $user->id) }}" class="btn btn-primary" style="color: #fff; height:30%">Add New Location</a></td>
                                    <td><a href="{{ route('admin.editForUpdate', $user->id) }}" class="btn btn-primary" style="color: #fff; height:40%">Update</a></td>
                                    <td> <form method="POST" action="{{ route('admin.destroy', $user->id) }}">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                       </form></td> 
                                </tr>
                                @endforeach
                      
                        </tbody>
                </table>
            </div>
           <!--- /here --->
           @include('includes.whatsapp')
    </div>
</section>

@endsection

@section('scripts')
<script src='{{ asset('assets/js/jquery-2.2.3.min.js') }}'></script>
<!-- //js  working-->
<!-- start-smoth-scrolling -->
<script src="{{ asset('assets/js/move-top.js') }}"></script>
<script src="{{ asset('assets/js/easing.js') }}"></script>
<script>
   jQuery(document).ready(function ($) {
       $(".scroll").click(function (event) {
           event.preventDefault();
           $('html,body').animate({
               scrollTop: $(this.hash).offset().top
           }, 1000);
       });
   });
</script>
<!-- start-smoth-scrolling -->
<!-- for-bottom-to-top smooth scrolling -->
<script>
   $(document).ready(function () {
       /*
           var defaults = {
           containerID: 'toTop', // fading element id
           containerHoverID: 'toTopHover', // fading element hover id
           scrollSpeed: 1200,
           easingType: 'linear' 
           };
       */
       $().UItoTop({
           easingType: 'easeOutQuart'
       });
   });
</script>
@endsection