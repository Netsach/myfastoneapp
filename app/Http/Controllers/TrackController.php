<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserLoding;
use App\Models\Location;

class TrackController extends Controller
{
    public function index () {
        return view("track");
    }
    public function trackData (Request $request) {
        $user = UserLoding::where('tracking_no', $request->tracking_no)->get();
        return view("trackData")->with('user', $user);
    }

    public function trackLocation ($id) {
        $users = Location::where('model_id', '=', $id)->get();
        return view('locationHistory', [
            'users' => $users
        ]);
    }
    
}
