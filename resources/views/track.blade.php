@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')
        <div class="buttom-w3">
            <h3 class="title">IT IS NEARLY THERE YET?</h3>
            <div class="news-info text-center">
            <div class="post">
            <form action="{{ route('trackData') }}" method="POST">
                @csrf
                <div class="letter">
                    <input type="text" name="tracking_no" placeholder="Enter your trackig no..." required="">
                </div>
                <div class="newsletter">
                    <input type="submit" name="submit" value="Track my parcel">
                </div>
            </form>
            </div>
            </div>
            @include('includes.whatsapp')
        </div>
@endsection