@extends('layouts.master')
@section('styles')

@endsection

@section('content')
@include('includes/slides_preview/second-banner')
<div class="bt">
    <img src="{{ asset('assets/images/origin.jpg') }}">
</div>
<!--//contact-->
    <div class="text-center1">
    Current state:<a href="#">Received Office</a>
    Mode of payment:<a href="#">Effective</a>
    </div>
    
<section id="tables" style="margin-left: 30%; margin-top:100px">
    <div class="page-header">
            {{-- <h3 class="bars" style="margin-left:10px; background:#ccc; width:10%; text-align:center;justify-content:center;align-items:center">
                <a href="{{ route('admin.dashboard') }}">Back</a></h3> --}}
            <div class="bs-docs-example" style="margin: 20px;">
                <table class="table" style="justify-content:center;align-items:center;
                width: 60%; border:1px solid #ccc">
                    <thead>
                       <tr>
                          <th style="padding-left: 50px;">Data</th>
                          <th style="padding-left: 50px;">Details</th>
                       </tr>
                    </thead>

                    <tbody>
                                @foreach ($user as $user)
                                <a href="{{ route('trackLocation', $user->id) }}" class="btn btn-primary">Current Locations</a>
                                <tr>
                                    <td><a href="">Tracking ID</a></td>
                                    <td><a href="">{{ $user->tracking_no  }}</a></td>
                                </tr>
                                <tr>
                                    <td><a href="">Delivery Schedule</a></td>
                                    <td><a href="">{{ $user->delivery_schedule }}</a></td>
                                </tr>
                                <tr>
                                    <td><a href="">Last Location </a></td>
                                    <td><a href="">{{ $user->last_location }}</a></td>
                                </tr>
                                    <tr>
                                        <td><a href="">Origin</a></td>
                                        <td><a href="">{{  $user->origin }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Destination</a></td>
                                        <td><a href="">{{  $user->destination }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Service Mode</a></td>
                                        <td><a href="">{{  $user->service_mode }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Weight</a></td>
                                        <td><a href="">{{  $user->weight }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Collection Date and Time</a></td>
                                        <td><a href="">{{  $user->collection_date_time }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Shipping Description</a></td>
                                        <td><a href="">{{  $user->shipping_description }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Type Service </a></td>
                                        <td><a href="">{{  $user->type_service }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Collection Date</a></td>
                                        <td><a href="">{{  $user->collection_date_time }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Name</a></td>
                                        <td><a href="">{{  $user->name1 }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Phone</a></td>
                                        <td><a href="">{{  $user->phone1 }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Address</a></td>
                                        <td><a href="">{{  $user->address1 }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Name of Recipient</a></td>
                                        <td><a href="">{{  $user->name2 }}</a></td>
                                    </tr>

                                    <tr>
                                        <td><a href="">Phone of Recipient</a></td>
                                        <td><a href="">{{  $user->phone2 }}</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="">Address of Recipient</a></td>
                                        <td><a href="">{{  $user->address2 }}</a></td>
                                    </tr>
                                    
                                @endforeach
                      
                        </tbody>
                </table>
            </div>
            <!--- /here --->
            @include('includes.whatsapp')
    </div>
</section>

@endsection

@section('scripts')
<script src='{{ asset('assets/js/jquery-2.2.3.min.js') }}'></script>
<!-- //js  working-->
<!-- start-smoth-scrolling -->
<script src="{{ asset('assets/js/move-top.js') }}"></script>
<script src="{{ asset('assets/js/easing.js') }}"></script>
<script>
   jQuery(document).ready(function ($) {
       $(".scroll").click(function (event) {
           event.preventDefault();
           $('html,body').animate({
               scrollTop: $(this.hash).offset().top
           }, 1000);
       });
   });
</script>
<!-- start-smoth-scrolling -->
<!-- for-bottom-to-top smooth scrolling -->
<script>
   $(document).ready(function () {
       /*
           var defaults = {
           containerID: 'toTop', // fading element id
           containerHoverID: 'toTopHover', // fading element hover id
           scrollSpeed: 1200,
           easingType: 'linear' 
           };
       */
       $().UItoTop({
           easingType: 'easeOutQuart'
       });
   });
</script>
@endsection