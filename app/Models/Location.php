<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = [
        'remark',
        'create_date',
        'state_shipping',
        'new_location',
        'track_no',
        'model_id',
    ];

}
