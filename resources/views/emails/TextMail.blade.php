This client contacted you...
<br>
**Name:** {{ $details['name'] }}
<br>
<br>
**Email:** {{ $details['email'] }} 
<br>
**Body:** {{ $details['body'] }}
<br>

Thank you again for choosing us.

Regards,<br>