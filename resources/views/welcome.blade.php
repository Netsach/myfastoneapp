@extends('layouts.master')
@section('content')
@include('includes/slides_preview/banner')
<div class="about" id="about">
    <div class="container">
       <div class="about-banner-grids ">
          <div class="col-md-4 left-of-about">
             <h3>What We Do</h3>
             <div class=" about-matter-left">
                <p> Fastone Express Cargo delivery service provides logistics and parcel delivery service to customers across the world. Our network of offices in several countries around the globe and our history of quality service delivery has made us the preferred choice in logistics and cargo delivery
                </p>
             </div>
             <div class="clearfix"></div>
          </div>
          <div class="col-md-4 about-pic">
          </div>
          <div class="col-md-4 right-of-about">
             <div class="about-airway colo">
                <p>How It Work
                </p>
             </div>
          </div>
          <div class="clearfix"></div>
       </div>
       <div class="col-md-6 about-right-info">
       </div>
       <div class=" col-md-6 about-info-air">
          <div class="air-list">
             <h4>Warehousing and Storage</h4>
             <p>Available in all our service locations around the globe.
             </p>
          </div>
          <div class="air-list">
             <h4>Explore Our Facilities</h4>
             <p>We operates fleet of more than 60 vehicles, from environmentally efficient push bikes for city center courier work, to motorcyclist, small vans and large vans and up to cargo planes, More than just a logistics company
             </p>
          </div>
          <div class="air-list">
             <h4>International Transport</h4>
             <p>Our network of offices in several countries around the globe and our history of quality service delivery has made us the preferred choice in logistics and cargo delivery
             </p>
          </div>
       </div>
    </div>
 </div>
 <!--// About-->
 <!-- services-->
 <div class="services" id="services">
    <h3 class="title clr">Services</h3>
    <div class="banner-bottom-girds">
       <div class="col-md-4 col-sm-6 col-xs-6 its-banner-grid gird-ser-clr2">
          <div class="white-shadow">
             <div class="white-left">
                <span class="fa fa-truck banner-icon" aria-hidden="true"></span>
             </div>
             <div class="white-right">
                <h4>Best Logistic</h4>
                <p>Our history of quality service delivery has made us the preferred choice in logistics and cargo delivery.</p>
             </div>
             <div class="clearfix"> </div>
          </div>
       </div>
       <div class="col-md-4 col-sm-6 col-xs-6 its-banner-grid gird-ser-clr">
          <div class="white-shadow">
             <div class="white-left">
                <span class="fa fa-clock-o banner-icon" aria-hidden="true"></span>
             </div>
             <div class="white-right">
                <h4>Fast Delivery</h4>
                <p>Fast delivery in all the various means of transportation all over the globe.</p>
             </div>
             <div class="clearfix"> </div>
          </div>
       </div>
       <div class="col-md-4 col-sm-6 col-xs-6 its-banner-grid colo">
          <div class="white-shadow">
             <div class="white-right">
                <div class=" white-left">
                   <span class="fa fa-home banner-icon" aria-hidden="true"></span>
                </div>
                <h4>Warehousing </h4>
                <p>Enough warehouse round the globe were our offices are located.</p>
             </div>
             <div class="clearfix"> </div>
          </div>
       </div>
       <div class="clearfix"> </div>
    </div>
 </div>
 <!--// services-->

   <!--partners-->
   <div class="parnters" id="parnters">
    <div class="container">
       <h3 class="title clr">Our Partners</h3>
       <div class="flexslider-info">
          <section class="slider side-side">
             <div class="flexslider">
                <ul class="slides">
                   <li>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/dhl1.png') }}" alt="logo1" class="img-responsive">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/fedex1.png')}}" alt="logo1" class="img-responsive">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/ups.png')}}" alt="logo1" class="img-responsive">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/after.png')}}" alt="logo1" class="img-responsive">
                      </div>
                   </li>
                   <li>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/dhl1.png')}}" alt="logo1" class="img-responsive">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/fedex1.png')}}" alt="logo1" class="img-responsive">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/ups.png')}}" alt="logo1" class="img-responsive">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3 usr-img">
                         <img src="{{ asset('assets/images/after.png')}}" alt="logo1" class="img-responsive">
                      </div>
                   </li>
                </ul>
             </div>
             <div class="clearfix"> </div>
          </section>

       </div>
       @include('includes.whatsapp')

       <div class="clearfix"> </div>
    </div>
 </div>
 <!--//partners -->
@endsection

@section('scripts')
<script  src='{{ asset('assets/js/jquery-2.2.3.min.js') }}'></script>
<!-- //js  working-->
<!-- banner-->
<script src="{{ asset('assets/js/responsiveslides.min.js') }}"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
      // Slideshow 4
    $("#slider4").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 900,
        namespace: "callbacks",
        before: function () {
          $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
          $('.events').append("<li>after event fired.</li>");
        }
      });
    
    });
 </script>

<script src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
<!--Start-slider-script-->
<script>
   $(window).load(function(){
       $('.flexslider').flexslider({
       animation: "slide",
       start: function(slider){
           $('body').removeClass('loading');
       }
       });
   });
</script>

<script src="{{ asset('assets/js/move-top.js') }}"></script>
<script src="{{ asset('assets/js/easing.js') }}"></script>
<script>
   jQuery(document).ready(function ($) {
       $(".scroll").click(function (event) {
           event.preventDefault();
           $('html,body').animate({
               scrollTop: $(this.hash).offset().top
           }, 1000);
       });
   });
</script>

<script>
    $(document).ready(function () {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
        */
        $().UItoTop({
            easingType: 'easeOutQuart'
        });
    });
 </script>
 
@endsection