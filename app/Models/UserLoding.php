<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoding extends Model
{
    use HasFactory;

    protected $table = 'user_loadings';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name1',
        'delivery_schedule',
        'last_location',
        'origin',
        'destination',
        'service_mode',
        'type_service',
        'weight',
        'collection_date_time',
        'shipping_description',
        'phone1',
        'address1',
        'name2',
        'phone2',
        'address2',
        'tracking_no',
        'user_id',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
