@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')
    <div class="contact" id="contact">
        <div class="container">
            <div class="container">
                <h3 class="title">Post New Location/ SHIPPING HISTORY</h3>
                <a href="{{ route('admin.dashboard') }}" class="btn btn-primary" style="color: #fff; height:30%">Back</a>
                @foreach ($user as $user)   
                <div class="col-md-12 contact-us">
                   <form method="POST" action="{{ route('admin.loadLocation') }}">
                     @include('includes.messages')
                    @csrf
                      <div class="styled-input">
                         <input type="hidden" name="id" value="{{ $user->id }}"  required="">
                      </div>
                      <div class="styled-input">
                        <input type="text" name="track_no" value="{{ $user->tracking_no }}" readonly>
                     </div>
                     <div class="styled-input">
                        <input type="text" name="new_location" placeholder="New location" required=""> 
                     </div>
                     <div class="styled-input">
                        <input type="text" name="state_shipping" placeholder="State shipping" required="">
                     </div>
                     {{-- <div class="styled-input">
                        <input type="text" name="create_date" placeholder="Date and time" required="">
                     </div> --}}
                     <div class="styled-input">
                        <input type="date" name="create_date" value="" placeholder="Date and time" required="" style="width: 100%; height: 50px;"> 
                     </div>
                     <div class="styled-input">
                        <input type="text" name="remark" placeholder="Remark" required="">
                     </div>
                     <br>
                     <div>
                        <div class="click">
                           <input type="submit" name="submit" value="Submit">
                        </div>
                     </div>
                   </form>
                </div>

                @endforeach
            </div>
            <!--- /here --->
            @include('includes.whatsapp')
            <div class="clearfix"> </div>
         </div>
    </div>
@endsection