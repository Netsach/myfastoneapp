@extends('layouts.master')

@section('content')
@include('includes/slides_preview/second-banner')

              <!--About-->
      <div class="about" id="about">
        <div class="container">
           <h3 class="title">About us</h3>
           <div class="about-inner-grids">
              <div class="col-md-5 col-sm-6 about-top-inner">
                 <div class="about-inner">
                    <ul>
                       <li>
                          <span class="fa fa-arrow-right dog-arrow" aria-hidden="true"></span>
              <p>fastone express cargo is one of the largest independent courier companies in the world. with an enviable reputation for delivering high quality. flexible and cost effective service to it clients. the business was founded 27 years ago and operates a fleet of more than 60 vehicles, from environmentally efficient push bikes for city center courier work, to motorcyclist, small vans and large vans and up to cargo planes, More than just a logistics company, fastone express cargo has developed a range of specialist logistic services for those looking for a total delivery solution patners or for those simple looking for Sameday Courier, Medicals Couriers or Specialised Distribution service. Whether you are a multi site corperation, a public sector organisation small local business or an individual, we have a solution to help improve efficiency and reduce costs</p>
                       </li>
                       
                    </ul>
                 </div>
              </div>
              <div class="col-md-7 col-sm-6 pope about-inner-img">
              </div>
              <div class="clearfix"> </div>
           </div>
        </div>
     </div>
     <!--//About-->
     <!-- counter-->
     <div class="static" id="rate">
        <div class="container">
           <div class="stats-info agileits w3layouts">
              <div class="col-md-3 col-sm-3 col-xs-3 agileits w3layouts stats-grid stats-grid-1">
                 <div class="ser-icone"> <span class="fa fa-plane font" aria-hidden="true"></span>
                 </div>
                 <div class=" agileits-w3layouts counter">3500</div>
                 <div class="stat-info-w3ls">
                    <h4 class="agileits w3layouts">Jets</h4>
                 </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 agileits w3layouts stats-grid stats-grid-2">
                 <div class="ser-icone"> <span class="fa fa-user font" aria-hidden="true"></span>
                 </div>
                 <div class=" agileits-w3layouts counter">650</div>
                 <div class="stat-info-w3ls">
                    <h4 class="agileits w3layouts ">Pilots</h4>
                 </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 stats-grid agileits w3layouts stats-grid-3">
                 <div class="ser-icone"> <span class="fa fa-cubes font" aria-hidden="true"></span>
                 </div>
                 <div class=" agileits-w3layouts counter">1021</div>
                 <div class="stat-info-w3ls">
                    <h4 class="agileits w3layouts ">Packing</h4>
                 </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 stats-grid agileits w3layouts stats-grid-4">
                 <div class="ser-icone"> <span class="fa fa-truck font" aria-hidden="true"></span>
                 </div>
                 <div class=" agileits-w3layouts counter">1010</div>
                 <div class="stat-info-w3ls">
                    <h4 class="agileits w3layouts">Logistics</h4>
                 </div>
              </div>
              @include('includes.whatsapp')
              <div class="clearfix"></div>
           </div>
        </div>
     </div>
@endsection

@section('scripts')
      <!--js working-->
      <script  src='{{ asset('assets/js/jquery-2.2.3.min.js') }}'></script>
      <!-- //js  working-->
      <!-- OnScroll-Number-Increase-JavaScript -->
      <script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
      <script src="{{ asset('assets/js/jquery.countup.js') }}"></script>
      <script>
         $('.counter').countUp();
      </script>
      <!-- //OnScroll-Number-Increase-JavaScript -->
      <!-- start-smoth-scrolling -->
      <script src="{{ asset('assets/js/move-top.js') }}"></script>
      <script src="{{ asset('assets/js/easing.js') }}"></script>
      <script>
         jQuery(document).ready(function ($) {
         	$(".scroll").click(function (event) {
         		event.preventDefault();
         		$('html,body').animate({
         			scrollTop: $(this.hash).offset().top
         		}, 1000);
         	});
         });
      </script>
      <!-- start-smoth-scrolling -->
      <!-- for-bottom-to-top smooth scrolling -->
      <script>
         $(document).ready(function () {
         	/*
         		var defaults = {
         		containerID: 'toTop', // fading element id
         		containerHoverID: 'toTopHover', // fading element hover id
         		scrollSpeed: 1200,
         		easingType: 'linear' 
         		};
         	*/
         	$().UItoTop({
         		easingType: 'easeOutQuart'
         	});
         });
      </script>
@endsection